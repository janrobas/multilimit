Multilimit
==========

This method takes array of arrays and a limit.

It returns new array of arrays such that the number of all elements in arrays doesn't exceed the limit.

It distributes the proportions.
If there are enough elements in all arrays, it takes similar amount of elements from all of them.
If there are not enough elements in some arrays, it takes more elements from others, so that the number of total elements approaches the specified limit.

It is useful for searchboxes which perform search in multiple categories, so we get results from all categories, but with a strict limit.

```
> multilimit.limitMultipleArrays([[1,2,3,4,5],[11,22,33,44,55,66], [222]], 9)
[ [ 1, 2, 3, 4 ], [ 11, 22, 33, 44 ], [ 222 ] ]

> multilimit.limitMultipleArrays([[1,2,3,4,5],[11,22,33,44,55,66], [222, 333]], 9)
[ [ 1, 2, 3 ], [ 11, 22, 33, 44 ], [ 222, 333 ] ]

> multilimit.limitMultipleArrays([[1,2,3,4,5],[11,22,33,44,55,66], [222, 333, 444]], 9)
[ [ 1, 2, 3 ], [ 11, 22, 33 ], [ 222, 333, 444 ] ]
```