// This function takes arrays (array of arrays) and limit (number).
// It returns new array of arrays, so that the length of all arrays combined is less or equal to the limit.
// It tries to take the same number of elements of each array. If one array is too short, it takes more elements out of other arrays.

exports.limitMultipleArrays = function (arrays, limit) {
    let limitedLengths = limitMultiple(arrays.map(x => x.length), limit);
    return arrays.map((x, i) => x.slice(0, limitedLengths[i]));

    // This function takes array of lengths and returns new array of lengths according to specified limit.
    function limitMultiple(arrayLengths, limit) {
        let result = arrayLengths.map(() => 0);
        let nonEmptyLengths = arrayLengths.filter(x => x > 0);
        let maxElementsToTake = Math.min(
            Math.min.apply(Math, nonEmptyLengths),
            Math.ceil(limit / nonEmptyLengths.length)
        );

        for (let i in arrayLengths) {
            if (arrayLengths[i] == 0)
                continue;

            arrayLengths[i] -= maxElementsToTake;
            result[i] += maxElementsToTake;
        }

        if (arrayLengths.reduce((a, b) => a + b) == 0)
            return result;

        let diff = limit - result.reduce((a, b) => a + b);

        if (diff > 0) {
            let add = limitMultiple(arrayLengths, diff);

            for (let i in arrayLengths) {
                result[i] += add[i];
            }
        } else if (diff < 0) {
            for (let i in arrayLengths) {
                if (result[i] == 0)
                    continue;

                result[i] -= 1;
                diff += 1;
                if (diff == 0)
                    break;
            }
        }

        return result;
    }
}